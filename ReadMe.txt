﻿PART 1:
_________________________________________________________________________________________

This project should take you 2 hours to complete. If you do not finish in 
2 Hours please send you code anyway. If you have any problems please 
contact us though your agency. The project relys on a free exchange service with a limit 
of 1000 request per month. If you find you are getting errors please register a new account
at the following site. https://openexchangerates.org/signup Click on the Forever Free 
link below the price options.

You can either Fork this repository and email us a link to the new fork or you can zip
and email the project back to us though your agency.

_________________________________________________________________________________________
*note: Methys.Helpers should be created and should contain the business logic

You have been asked to take over this project, the previous developer did not 
check in all he code and the project is in a broken state. You have been asked
to 


1. Take over the project and get it back into a working state, The project will 
be considered in a working a working state when it compiles and is able
to display the current exchange rate to the dollar

eg. "On 11 of July 2016 $1 will buy you R12.40"


2. Improve or comment on any of the coding practices used, Coding standers 
Error checking, and as asked if you can add some unit tests to the project.
(You do not need to add more than 2 Unit tests)

3. The client has requested that you update the application to also display 
how many Rands a pound will buy.

NOTE : The client has expressed concerns about the pound exchange rate,
he feels the values are not correct and suggests you confirm them.

NOTE : The exchange rate is based off in Dollars. As in Dollars to rand, 
Dollars to Pound. You will need to take this into account to calculate
Pounds to Rands.

4. Lastly The Client does not want to pay for the Exchange service but
would like to see the Max, Min and Avg price for the rand compared to the 
dollar for a given month.

To achieve this you will need to store historic values on the local machine.

(In the spirit of keeping this sample project short you may assume that you 
only need to process the last full month, and save your values in a text file. You may then
read from this file and use it to get the values for may.)

The output should look something like this and follow the current exchange rate.

"For the Month of May $1 would buy you a Max of {value} Rands and a Min of {value}
The average for the month was {value}"

								----------

PART 2:

This project is focussed on front end. There is no time limit on this, except that we ask you be honest 
in keeping track of how much time you spent on this part and report this back to us.

Tasks:

1. A designer has created the css and you need to implement this into the existing application

2. Implement the login partial view so that it is displayed in all pages

3. Implement Ajax call to retrieve data from server and display it in a control. 
https://datatables.net/ must be implemented and used to display the records

4. I want to be able to edit only the occupation field of the employees.
Changes made must be saved by means of writing to a textfile in the DataFiles folder on the server

_________________________________________________________________________________________

When complete, please zip the whole solution, and email it back to us.
