﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Methys.UI_Assessment.Startup))]
namespace Methys.UI_Assessment
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
