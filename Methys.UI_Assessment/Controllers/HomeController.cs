﻿using Methys.UI_Assessment.BA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Methys.UI_Assessment.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BackEndInteraction()
        {
            ViewBag.Message = "Do AJAX call to get data from backend";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult GetAllEmployees()
        {
            EmployeeDomain d = new EmployeeDomain();
            var model = d.getAllActiveEmployeesList();
            return new JsonResult() { Data = model, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}