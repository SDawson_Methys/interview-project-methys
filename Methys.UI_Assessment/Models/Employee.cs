﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Methys.UI_Assessment.Models
{
    public class Employee
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Occupation { get; set; }
    }
}