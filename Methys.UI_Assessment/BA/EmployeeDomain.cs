﻿using Methys.UI_Assessment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Methys.UI_Assessment.BA
{
    public class EmployeeDomain
    {
        public List<Employee> getAllActiveEmployeesList()
        {
            List<Employee> returnList = new List<Employee>();
            Employee e = new Employee();

            e.ID = Guid.NewGuid();
            e.Name = "David";
            e.Occupation = "Tester";
            returnList.Add(e);

            e.ID = Guid.NewGuid();
            e.Name = "Peter";
            e.Occupation = "Testing Team Leader";
            returnList.Add(e);

            e.ID = Guid.NewGuid();
            e.Name = "Bill";
            e.Occupation = "Developer";
            returnList.Add(e);

            e.ID = Guid.NewGuid();
            e.Name = "Gary";
            e.Occupation = "Developer";
            returnList.Add(e);

            e.ID = Guid.NewGuid();
            e.Name = "Tim";
            e.Occupation = "Business Analyst";
            returnList.Add(e);

            e.ID = Guid.NewGuid();
            e.Name = "Sue";
            e.Occupation = "Human Resources";
            returnList.Add(e);

            e.ID = Guid.NewGuid();
            e.Name = "Jason";
            e.Occupation = "General Manager";
            returnList.Add(e);

            return returnList;
        }
    }
}