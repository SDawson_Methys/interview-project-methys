﻿using Methys.DAO.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methys.DAO
{
    public class DAO 
    {
        public StockDao Stock { get; set; }

        public DAO(StockDao stockDao)
        {
            this.Stock = stockDao;
        }

    }
}
