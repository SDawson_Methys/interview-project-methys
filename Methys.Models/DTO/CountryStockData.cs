﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methys.Models.DTO
{
    public class CountryStockData
    {
        public decimal AED {get; set;}
        public decimal AFN {get; set;}
        public decimal ALL {get; set;}
        public decimal AMD {get; set;}
        public decimal GDP {get; set;}
        public decimal YER {get; set;}
        public decimal ZAR {get; set;}
        public decimal ZMK {get; set;}
        public decimal ZWL {get; set;}
    }
}
